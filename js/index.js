<script>
		 $(function(){
		 	$("[data-toggle='tooltip']").tooltip();
		 	$("[data-toggle='popover']").popover();
		 	$('.carousel').carousel({
  				interval: 4000
				}); 
		 	$("#contacto").on('show.bs.modal',function(e){
		 		console.log('el modal contacto se esta mostrando');
		 	$('#Contactobtn').removeClass('btn-outline-success');
		 	$('#Contactobtn').addClass('btn-primary');
		 	$('#Contactobtn').prop('disabled',true);
		 	});
		 	$("#contacto").on('shown.bs.modal',function(e){
		 		console.log('el modal contacto se mostró');
		 	});
		 	$("#contacto").on('hide.bs.modal',function(e){
		 		console.log('el modal contacto se esta ocultando');
		 	});
		 	$("#contacto").on('hidden.bs.modal',function(e){
		 		console.log('el modal contacto se ocultó');
		 	$('#Contactobtn').prop('disabled',false);
		 	});
		 });
	</script>